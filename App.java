import blockchain.Blockchain;
import models.CreateWalletReq;
import utils.SecUtils;
import blockchain.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

class App {

  public static void main(String[] args) {
    String privateKey;
    SecUtils secUtils = new SecUtils();
//    privateKey = secUtils.GeneratePrivateKey("HelloB4839");
//    System.out.println("private key hash:" + privateKey);
    Blockchain blockchain = new Blockchain();
    Wallet wallet = new Wallet();
    CreateWalletReq createWalletReq =  new CreateWalletReq();
    createWalletReq.VaultOpenDate = "27, jan 16, 2022";
    createWalletReq.WalletName = "don telma";
    createWalletReq.WalletVault = true;
    createWalletReq.Password = "12345";
    createWalletReq.VaultLimit = 100;
    createWalletReq.LimitTime = "d";
    wallet.CreateWallet(createWalletReq);
//    blockchain.GetChain("JSX1A1yfNqtCP1XYELFiaatL9PYeR8wDk38pH+nuX8U=");
//       System.out.println(blockchain.GetBalance("JSX1A1yfNqtCP1XYELFiaatL9PYeR8wDk38pH+nuX8U="));
//      blockchain.GetLastBlock("R43ehRegT13C9gyPbFaMfGCF1lRJwcvyGI1f2DTbkgw=");
//      if (blockchain.VerifyLastBlock("R43ehRegT13C9gyPbFaMfGCF1lRJwcvyGI1f2DTbkgw=")){
//          System.out.println("Yes block chain verified");
//      }else {
//          System.out.println("Error verifying blockchain");
//      }

      // ...........................................Transfer .......................................
//      try {
//          blockchain.Transfer("0TwQ33SS1Qxa1YWJ7qCZuY7uF0B6v6yYUMEeRJMSJS4=",
//                  "JSX1A1yfNqtCP1XYELFiaatL9PYeR8wDk38pH+nuX8U=", 1);
//      } catch(Exception e){
//          System.out.println("Error transfering coins .....  "+ e);
//      }

  }


  public void startServer(){
    try (ServerSocket serverSocket = new ServerSocket(4000)) {

      // int SDK_INT = android.os.Build.VERSION.SDK_INT;
      // if (SDK_INT > 8) {
      //     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
      //             .permitAll().build();
      //     StrictMode.setThreadPolicy(policy);
      // }
      // InetAddress IP=InetAddress.getLocalHost();
      // Log.d("SERVER","ip address is :"+ IP.getHostAddress());
      // System.out.println("Server is listening on port " + 4000);
      // Log.d("SERVER","Server is listening on port " + 4000 );

      while (true) {
          Socket socket = serverSocket.accept();

          System.out.println("New client connected");
          // Log.d("SERVER", "New client connected");

          OutputStream output = socket.getOutputStream();
          PrintWriter writer = new PrintWriter(output, true);

          writer.println("kdkmd");
      }
    } catch (IOException ex) {
        System.out.println("Server exception: " + ex.getMessage());
        // Log.d("SERVER","Server exception: "+ ex.getMessage());
        ex.printStackTrace();
    }
  }
}
