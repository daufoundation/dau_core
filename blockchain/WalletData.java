package blockchain;

import java.io.Serializable;

public class WalletData implements Serializable {
    public String Address;
    public String CreatedAt;
    public String Name;
    public boolean Vault;
    public float VaultLimit;
    public String VaultOpenDate;
    public String LimitTime;
}