package blockchain;


import java.io.Serializable;

class BlockData implements Serializable {
    public String ID;
    public String Hash;
    public String PrevHash;
    public float Amount;
    public String Sender;
    public String Reciever;
    public String CreatedAt;
    public String PrivateKeyHash;
}
public class Block {
    public String ID;
    public String Hash;
    public String PrevHash;
    public String Amount;
    public String Sender;
    public String Reciever;

}
