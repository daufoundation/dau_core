package blockchain;

import utils.CommonUtils;
import utils.FileUtils;
import utils.SecUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Blockchain {
    SecUtils secUtils = new SecUtils();
    CommonUtils commonUtils = new CommonUtils();
    public ArrayList<BlockData> blockchain;
    FileUtils fileUtils = new FileUtils();
    // this creates a new blocklist for a new wallet
    public void NewChain(String address, String password){
        blockchain = new ArrayList<>();
        FileUtils fileUtils = new FileUtils();
        // generate new block
        BlockData blockData = NewGenesisBlock(address, password);
        // add block to chain
        blockchain.add(blockData);
        fileUtils.CreateFileObject(blockchain, "data/"+address+"/chain.bin");
    }

    // this creates a new block in the chain
    public BlockData NewGenesisBlock(String address, String password){
        BlockData blockData = new BlockData();
        blockData.Amount = 100;
        blockData.ID = secUtils.GeneratePrivateKey(secUtils.GenerateRandString());
        blockData.Sender = "0000000";
        blockData.Reciever = address;
        blockData.CreatedAt = commonUtils.CurrentDate();
        blockData.PrevHash = "0000000";
        blockData.PrivateKeyHash = secUtils.GeneratePrivateKey(password);
        blockData.Hash = secUtils.GenerateHash(GetBlockHString(blockData,address));
        return blockData;
    }

    public String GetBlockHString(BlockData block, String address){
        return address+block.Sender+block.Reciever+block.PrevHash;
    }

    public void AddBlock(BlockData block, String address){
        ArrayList<BlockData> blocks = new ArrayList<BlockData>(List.of(GetChain(address))) ;
        blocks.add(block);
        fileUtils.CreateFileObject(blocks, "data/"+address+"/chain.bin");
    }

    public BlockData[] GetChain(String address) {
        Object obj =fileUtils.ReadFileObj("data/"+address+"/chain.bin");
        ArrayList<BlockData> bkdata = new ArrayList<BlockData>();
        bkdata = (ArrayList) obj;
        for (BlockData block : bkdata ){
            System.out.println(block.Hash);
            System.out.println(block.Reciever);
            System.out.println(block.Amount);
            System.out.println(block.CreatedAt);
        }
        return bkdata.toArray(new BlockData[0]);
    }


    public BlockData GetLastBlock(String address) {
        Object obj =fileUtils.ReadFileObj("data/"+address+"/chain.bin");
        ArrayList<BlockData> bkdata = new ArrayList<BlockData>();
        bkdata = (ArrayList) obj;
        BlockData[] blocks = bkdata.toArray(new BlockData[0]);
        BlockData lastBlock = blocks[blocks.length-1];
        System.out.println(lastBlock.Hash);
        System.out.println(lastBlock.Reciever);
        return lastBlock;
    }

    public boolean VerifyLastBlock(String address){
        Object obj =fileUtils.ReadFileObj("data/"+address+"/chain.bin");
        ArrayList<BlockData> bkdata = new ArrayList<BlockData>();
        bkdata = (ArrayList) obj;
        BlockData[] blocks = bkdata.toArray(new BlockData[0]);
        BlockData lastBlock = blocks[blocks.length-1];
        System.out.println(lastBlock.Hash);
        System.out.println(lastBlock.Reciever);

        if (blocks.length > 1){
            BlockData prevBlockData =  blocks[blocks.length-2];
            String hash = secUtils.GenerateHash(GetBlockHString(prevBlockData, address));

            if (lastBlock.PrevHash == hash ){
                return true;
            }else {
                return false;
            }
        }

        if (blocks.length == 1){
//            System.out.println("One block found ");
            String hash = secUtils.GenerateHash(GetBlockHString(lastBlock, address));
//            System.out.println("hash "+hash);
//            System.out.println("last block hash "+lastBlock.Hash);
            if (Objects.equals(lastBlock.Hash, hash)) {
                return true;
            }else {
                return false;
            }
        }

        return false ;

    }

    public float GetBalance(String address){
        BlockData[] blocks = GetChain(address);
        float balance = 0;
        for (BlockData block:blocks){
            if (Objects.equals(block.Sender, address)){
                balance = balance - block.Amount ;
            }
            if (Objects.equals(block.Reciever, address)){
                balance = balance + block.Amount;
            }
        }
        return balance;
    }


    // this function transfers money from one wallet to another
    public void Transfer(String senderAddress, String recieverAddress, float amount ) throws Exception {
        Error error = null;
        // check if sender has the amount of money in question
        if (!(GetBalance(senderAddress) >= amount)){
            throw new Exception("Insufficient balance");

        }
        BlockData senderLastBlock = GetLastBlock(senderAddress);
        BlockData recieverLastBlock = GetLastBlock(recieverAddress);

        // new blocks
        BlockData senderBlock = new BlockData();
        BlockData recieverBlock = new BlockData();

        senderBlock.Sender = senderAddress;
        senderBlock.Reciever = recieverAddress;
        senderBlock.Amount = amount;
        senderBlock.CreatedAt = commonUtils.CurrentDate();
        senderBlock.PrevHash = senderLastBlock.Hash;
        senderBlock.ID = secUtils.GenerateHash(secUtils.GenerateRandString());
        senderBlock.PrivateKeyHash = senderLastBlock.PrivateKeyHash;
        senderBlock.Hash = secUtils.GenerateHash(GetBlockHString(senderBlock, senderAddress));

        recieverBlock.Sender = senderAddress;
        recieverBlock.Reciever = recieverAddress;
        recieverBlock.Amount = amount;
        recieverBlock.ID = secUtils.GenerateHash(secUtils.GenerateRandString());
        recieverBlock.PrevHash = recieverLastBlock.Hash;
        recieverBlock.CreatedAt = commonUtils.CurrentDate();
        recieverBlock.PrivateKeyHash = recieverLastBlock.PrivateKeyHash;
        recieverBlock.Hash = secUtils.GenerateHash(GetBlockHString(recieverBlock, recieverAddress));

        // add new blocks to the chain
        AddBlock(senderBlock, senderAddress);
        AddBlock(recieverBlock, recieverAddress);

    }


    public boolean validateVault(WalletData walletData, float Amount){
        if (!walletData.Vault){
            return false;
        }
        // check if  the limit has been reached
        return true;
    }

}
