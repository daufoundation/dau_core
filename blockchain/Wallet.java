package blockchain;


import models.CreateWalletReq;
import utils.FileUtils;
import utils.SecUtils;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;


public class Wallet {
    public String Address;
    public String CreatedAt;
    public String Name;
    FileUtils fileUtils = new FileUtils();
    SecUtils secUtils = new SecUtils();

    public void CreateWallet(CreateWalletReq walletReq){
        String wfName = "";
        String walletName = "";

        // gnenrating wallet address
        Random r = new Random();
        for (var i=0;i<32; i++ ){
            char randomChar = (char) (97 + r.nextInt(16));
            wfName = wfName+ randomChar;
        }

        walletName = secUtils.GenerateHash(wfName);

        // create a folder in data
        File dir = new File("data/"+walletName+"/");
        dir.mkdirs();
        if (!dir.exists()){

        }

        WalletData walletData = new WalletData();
        walletData.Address = walletName;
        walletData.Name = walletReq.WalletName;
        if (walletReq.WalletVault){
            walletData.Vault = true;
            walletData.VaultLimit = walletReq.VaultLimit;
            walletData.VaultOpenDate = walletReq.VaultOpenDate;
            walletData.LimitTime = walletReq.LimitTime;
        }


        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        walletData.CreatedAt =formatter.format(date);

        fileUtils.CreateFileObject(walletData, "data/"+walletName+"/data.bin");

        // create new blockchain data
        Blockchain blockchain = new Blockchain();
        blockchain.NewChain(walletName, walletReq.Password);


//        WalletData walletx = (WalletData) obj;
//        System.out.println("Read wallet address :"+walletx.Address);
//        System.out.println("Read wallet name :"+walletx.Name);
//        System.out.println("Read wallet time :"+walletx.CreatedAt);
    }
}
