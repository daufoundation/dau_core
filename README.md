# dau_core

Dau is a java-based blockchain  built to run on Linux, windows, android. Dau core is a peer to peer payment network. It is an open source technology for a global comunity of traders, gamers, and everyone is welcome. 

# How to get started 
- clone the repo 
- download intellij IDE
- compile and run. Java 7+ is recommended
- compile into .jar and link with other applications eg. Android 
- make changes and build jar file (default : out/artifacts/dau_core_jar)
- run `run.sh` (this moves the new build into test folders ./test_servers )
- startup 4 terminals, configure each test server via a json or env file
- Now you have 4 server running, a simulation of a decentralized system
- connect with each server via local host but on different ports over a tcp network. Use netcat on linux or WSL

# Development Goals
- Launch mobile nodes v1 by 2022 March 
- 1,000 minners by december 2022
- Light weight smart contracts that can be executed on mobile nodes 2023
- Security tests and updates 2023
- Review on our proof of stake implementation 2023
- Launch version 2 of dau core 2023 March 
- Business wallets, business varifications and identification for safer business transactions above $100M worth of DAU


# The DAU foundation 
The DAU foundation currently in Rwanda is responsible for the development and management of the DAu projects. Checkout www.daufoundation.com 

# How to contribute
- Join the discord community https://discord.gg/HsjZV3ptvE
- Cone the repo and go through the code base
- Look for issues on the gitlab issues section 
- Look for reported bugs and reproduce them 
- Post your intentions to work on a feature on discord and gitlab 
- reach out to other developers on discord if you need to have a video call and get help
- Thanks for joining us



